Rails.application.routes.draw do

  get '/share/:name' => 'application#share'

  post '/api/get_question' => 'application#get_question'
  post '/api/set_question' => 'application#set_question'
  post '/api/send_form' => 'application#send_form'
  post '/api/minus_score' => 'application#minus_score'

  root "index#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
