set :repo_url, 'git@bitbucket.org:Mirkom63/michelin.git'
set :application, 'michelin'
application = 'michelin'
set :rvm_type, :user
set :rvm_ruby_version, '2.7.0'
set :deploy_to, "/var/www/tco-savefuel.ru/"


set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle  public/assets public/system public/videos public/audio }

set :linked_files, %w{config/secrets.yml config/database.yml config/boot.rb}


namespace :setup do
  desc 'Загрузка конфигурационных файлов на удаленный сервер'
  task :upload_config do
    on roles :all do
      execute :mkdir, "-p #{shared_path}"
      ['shared/config', 'shared/run'].each do |f|
        upload!(f, shared_path, recursive: true)
      end
    end
  end
end

namespace :nginx do
  desc 'Создание симлинка в /etc/nginx/conf.d на nginx.conf приложения'
  task :append_config do
    on roles :all do
      sudo :ln, "-fs #{shared_path}/config/nginx.conf /etc/nginx/conf.d/#{fetch(:application)}.conf"
    end
  end
  desc 'Релоад nginx'
  task :reload do
    on roles :all do
      sudo :service, :nginx, :reload
    end
  end
  desc 'Рестарт nginx'
  task :restart do
    on roles :all do
      sudo :service, :nginx, :restart
    end
  end
  after :append_config, :restart
end

set :unicorn_config, "#{shared_path}/config/unicorn.rb"
set :unicorn_pid, "#{shared_path}/run/unicorn.pid"

namespace :application do
  desc 'Запуск Unicorn'
  task :start do
    on roles(:app) do
      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec unicorn_rails -c #{fetch(:unicorn_config)} -E #{fetch(:rails_env)} -D"
    end
  end
  desc 'Завершение Unicorn'
  task :stop do
    on roles(:app) do
      execute "if [ -f #{fetch(:unicorn_pid)} ] && [ -e /proc/$(cat #{fetch(:unicorn_pid)}) ]; then kill -9 `cat #{fetch(:unicorn_pid)}`; fi"

      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec rake db:migrate RAILS_ENV=production"
    end
  end
end


namespace :deploy do

ask(:message, "Commit message?")


  after :finishing, 'application:stop'
  after :finishing, 'application:start'
  after :finishing, :cleanup
end
