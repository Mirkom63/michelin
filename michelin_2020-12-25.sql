# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 127.0.0.1 (MySQL 5.7.29)
# Схема: michelin
# Время создания: 2020-12-25 19:25:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы ar_internal_metadata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ar_internal_metadata`;

CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`)
VALUES
	('environment','development','2020-06-29 14:44:48','2020-06-29 14:44:48');

/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы schema_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;

INSERT INTO `schema_migrations` (`version`)
VALUES
	('20200701113349');

/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы test_results
# ------------------------------------------------------------

DROP TABLE IF EXISTS `test_results`;

CREATE TABLE `test_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `browser_id` varchar(255) DEFAULT NULL,
  `test_1` tinyint(1) DEFAULT NULL,
  `test_2` tinyint(1) DEFAULT NULL,
  `test_3` tinyint(1) DEFAULT NULL,
  `test_4` tinyint(1) DEFAULT NULL,
  `test_5` tinyint(1) DEFAULT NULL,
  `test_6` tinyint(1) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `test_results` WRITE;
/*!40000 ALTER TABLE `test_results` DISABLE KEYS */;

INSERT INTO `test_results` (`id`, `browser_id`, `test_1`, `test_2`, `test_3`, `test_4`, `test_5`, `test_6`, `score`, `phone`, `email`, `created_at`, `updated_at`)
VALUES
	(59,'150937637191',1,1,1,1,0,NULL,23340000,'1 (231) 231-23-12,','petrovaleksandr1992@list.ru,','2020-08-03 13:59:49','2020-08-03 14:00:32'),
	(60,'158060731337',1,0,0,0,1,NULL,11240000,NULL,NULL,'2020-08-03 14:01:24','2020-08-03 14:01:34'),
	(61,'641068454439',1,1,1,1,0,NULL,23340000,NULL,NULL,'2020-08-03 14:02:22','2020-08-03 14:02:36'),
	(62,'15767300616',0,1,1,1,1,NULL,24590000,NULL,'petrovaleksandr1992@list.ru,petrov_smile@list.ru,','2020-08-03 14:02:46','2020-08-03 14:03:28'),
	(63,'216080884776',1,NULL,NULL,NULL,NULL,NULL,12900000,NULL,NULL,'2020-08-03 14:03:47','2020-08-03 14:03:50'),
	(64,'331445613131',NULL,1,0,NULL,NULL,NULL,11040000,NULL,NULL,'2020-08-03 14:03:52','2020-08-03 14:04:02'),
	(65,'928617338889',NULL,NULL,NULL,0,0,NULL,6400000,NULL,NULL,'2020-08-03 14:04:05','2020-08-03 14:04:36'),
	(66,'661884087101',1,NULL,NULL,NULL,NULL,NULL,12900000,NULL,NULL,'2020-08-03 14:04:46','2020-08-03 14:04:50'),
	(67,'58706154288',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-03 14:04:53','2020-08-03 14:04:53'),
	(68,'859844947262',1,1,1,NULL,NULL,NULL,22440000,NULL,NULL,'2020-08-03 14:05:00','2020-08-03 14:05:27'),
	(69,'141332436062',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 16:46:05','2020-08-04 16:46:05'),
	(70,'804173115780',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 16:46:48','2020-08-04 16:46:48'),
	(71,'997661187900',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 16:46:56','2020-08-04 16:47:42'),
	(72,'258816490955',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 16:50:34','2020-08-04 16:50:41'),
	(73,'338848883505',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 16:50:49','2020-08-04 16:50:55'),
	(74,'980202614912',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 16:52:01','2020-08-04 16:52:07'),
	(75,'673514945739',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 16:54:04','2020-08-04 16:54:04'),
	(76,'833053976385',1,NULL,NULL,NULL,NULL,NULL,12900000,NULL,NULL,'2020-08-04 16:54:42','2020-08-04 16:54:44'),
	(77,'177352254906',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 16:56:09','2020-08-04 16:56:19'),
	(78,'391560928847',0,0,1,NULL,NULL,NULL,17650000,NULL,NULL,'2020-08-04 16:57:31','2020-08-04 16:57:45'),
	(79,'688034087571',0,0,1,0,NULL,NULL,13090000,NULL,NULL,'2020-08-04 16:58:12','2020-08-04 16:58:26'),
	(80,'511608698626',0,0,1,NULL,NULL,NULL,17650000,NULL,NULL,'2020-08-04 16:58:28','2020-08-04 16:58:37'),
	(81,'273094357409',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 17:53:59','2020-08-04 17:54:07'),
	(82,'758635099857',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 17:56:10','2020-08-04 17:56:18'),
	(83,'623680682816',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 17:56:36','2020-08-04 17:56:43'),
	(84,'789541556795',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 17:56:59','2020-08-04 17:57:05'),
	(85,'200715457772',0,0,0,NULL,NULL,NULL,6090000,NULL,NULL,'2020-08-04 17:57:59','2020-08-04 17:58:07'),
	(86,'117679821780',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 17:58:36','2020-08-04 17:58:45'),
	(87,'444717033602',0,0,0,NULL,NULL,NULL,8650000,NULL,NULL,'2020-08-04 17:59:47','2020-08-04 17:59:54'),
	(88,'513544933157',0,0,0,NULL,NULL,NULL,6090000,NULL,NULL,'2020-08-04 18:00:09','2020-08-04 18:00:15'),
	(89,'925571255315',0,0,1,0,NULL,NULL,15650000,NULL,NULL,'2020-08-04 18:01:37','2020-08-04 18:01:47'),
	(90,'602502660255',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 18:01:58','2020-08-04 18:02:06'),
	(91,'313635738161',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 18:04:09','2020-08-04 18:04:16'),
	(92,'281287187022',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 18:04:41','2020-08-04 18:04:47'),
	(93,'656153397806',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 18:06:08','2020-08-04 18:06:15'),
	(94,'264517715822',0,0,1,0,1,NULL,16240000,NULL,NULL,'2020-08-04 18:07:24','2020-08-04 18:07:51'),
	(95,'17966906010',1,0,0,NULL,NULL,NULL,10090000,NULL,NULL,'2020-08-04 18:07:55','2020-08-04 18:08:07'),
	(96,'20566594121',1,0,1,NULL,NULL,NULL,21650000,NULL,NULL,'2020-08-04 18:20:20','2020-08-04 18:20:31'),
	(97,'776325434816',1,NULL,NULL,NULL,NULL,NULL,12900000,NULL,NULL,'2020-08-04 18:22:04','2020-08-04 18:22:08'),
	(98,'970325819210',1,0,1,NULL,NULL,NULL,19090000,NULL,NULL,'2020-08-04 18:22:20','2020-08-04 18:22:35'),
	(99,'824478776788',0,NULL,NULL,NULL,NULL,NULL,8900000,NULL,NULL,'2020-08-04 18:29:25','2020-08-04 18:29:27'),
	(100,'354439010332',0,0,1,NULL,NULL,NULL,15090000,NULL,NULL,'2020-08-04 18:31:51','2020-08-04 18:31:57'),
	(101,'8380963459',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 18:35:10','2020-08-04 18:35:10'),
	(102,'260874546502',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 19:59:12','2020-08-04 19:59:30'),
	(103,'870644070561',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 20:03:51','2020-08-04 20:04:01'),
	(104,'241687241115',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 20:06:57','2020-08-04 20:07:06'),
	(105,'287967283033',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 20:08:28','2020-08-04 20:08:28'),
	(106,'292482266912',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 20:08:38','2020-08-04 20:08:48'),
	(107,'693749939576',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 20:10:20','2020-08-04 20:10:33'),
	(108,'23088795577',0,0,1,0,0,NULL,10990000,NULL,NULL,'2020-08-04 20:11:08','2020-08-04 20:11:22'),
	(109,'670855356609',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-08-04 20:11:38','2020-08-04 20:11:38'),
	(110,'47851420965',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-11-10 13:19:58','2020-11-10 13:19:58'),
	(111,'412783876233',NULL,NULL,NULL,NULL,NULL,NULL,10500000,NULL,NULL,'2020-11-10 13:20:15','2020-11-10 13:20:15');

/*!40000 ALTER TABLE `test_results` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
