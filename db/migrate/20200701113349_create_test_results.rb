class CreateTestResults < ActiveRecord::Migration[5.2]
  def change
    create_table :test_results do |t|
      t.string :browser_id
      t.boolean :test_1
      t.boolean :test_2
      t.boolean :test_3
      t.boolean :test_4
      t.boolean :test_5
      t.boolean :test_6
      t.integer :score
      t.string :phone
      t.string :email
      t.timestamps
    end
  end
end
