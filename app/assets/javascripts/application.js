// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery_ujs

//=require plugin/jquery.mask.js
//=require plugin/jquery.scrollbar.min.js
//=require plugin/jquery.scrollTo.min.js



$(document).ready(function(){



  $(document).on('click','.js-click-vk',function(){
    ym(57516034,'reachGoal','button7_vk');
  });
  $(document).on('click','.js-click-fb',function(){
    ym(57516034,'reachGoal','button8_fb');
  });

  $(document).on('click','.js-copy-url',function(){
		var url=$(this).parent().find('input').val();

		var $tmp = $("<textarea>");
		$("body").append($tmp);
		$tmp.val(url).select();
		document.execCommand("copy");
		$tmp.remove();

		alert('Ссылка скопирована в буфер обмена');

	});


  $('.form_template input').keyup(function(){
    $(this).css('background','#fff');
  });
  $('.form_phone input').mask('0 (000) 000-00-00');

  $('.form_template').submit(function(e){
    e.preventDefault();

    var input=$(this).find('input').eq(0);
    var type=input.attr('type')
    var value=input.val();

    check_form=true;

    if(value.length==0){
      $(this).parent().find('input').css('background','#A6192E');
    }else{

      var button=$(this).closest('.form_block').find('.button');

      $('.js-pop-up-forms .form_title').html('Отправка...');
      button.prop('disabled',true).text('...');

      setTimeout(function(){
        $('.js-pop-up-forms .form_title').html('Спасибо!<br>Данные получены!');
        button.text('Готово!');


        setTimeout(function(){
          $('.js-pop-up-forms').hide();
          $('.js-pop-up').css('display','flex');
          button.prop('disabled',false).text('Отправить');
          $('.form_template input').val('');
        },2000);

      },2000);




      $.ajax({
        type     :'POST',
        cache    : false,
        data: $(this).serialize()+'&browser_id='+browser_id,
        url  : '/api/send_form',
        success  : function(response) {
        },
      });


      if($(this).find('input[name="phone"]').val()==undefined){
        ym(57516034,'reachGoal','button6_email');
      }else{
        ym(57516034,'reachGoal','button5_number');
      }

    }


  });


  $('.form_opd_input').click(function(){
    if($(this).is('.active')){
      $(this).removeClass('active');
      $(this).closest('.form_block').find('.button').addClass('button_disable').prop('disabled',true);
    }else{
      $(this).addClass('active');
      $(this).closest('.form_block').find('.button').removeClass('button_disable').prop('disabled',false);
    }
  });

  $(document).on('click','.js-open-form-calc',function(){
    $('.js-pop-up-forms').css('display','flex');
    $('.js-pop-up').hide();
    $('.form_thanks').hide();
    $('.form_content').show();
    $('.js-pop-up-forms .form_title').html('Как удобнее получить<br> подробности об экономии?');
    $('.buttons_form').css('display','flex').find('.button').attr('type','calc');
    $('.form_block').hide();
    $('input[name="type_form"]').val('calc');


    ym(57516034,'reachGoal','button3_economic');
  });
  $(document).on('click','.js-open-form-pdf',function(){
    $('.js-pop-up-forms').css('display','flex');
    $('.js-pop-up').hide();
    $('.form_thanks').hide();
    $('.form_content').show();
    $('.js-pop-up-forms .form_title').html('Как вам удобнее<br> получить PDF?');
    $('.buttons_form').css('display','flex').find('.button').attr('type','pdf');
    $('.form_block').hide();
    $('input[name="type_form"]').val('pdf');

    ym(57516034,'reachGoal','button2_pdf');
  });

  $(document).on('click','.js-open-form-consult',function(){
    $('.js-pop-up-forms').css('display','flex');
    $('.js-pop-up').hide();
    $('.form_thanks').hide();
    $('.form_content').show();
    $('.js-pop-up-forms .form_title').html('Введите телефон и<br> наш менеджер<br> перезвонит Вам:');
    $('.buttons_form').hide();
    $('.form_block').show();
    $('.form_template').hide();
    $('.form_phone').show();
    $('input[name="type_form"]').val('consult');

    ym(57516034,'reachGoal','button4_consultation');
  });

  $('.button_finish_sms').click(function(){
    $('.js-pop-up-forms .form_title').html('Введите ваш номер телефона:');
    $('.buttons_form').hide();
    $('.form_block').show();
    $('.form_template').hide();
    $('.form_phone').show();
  });

  $('.button_finish_email').click(function(){
    $('.js-pop-up-forms .form_title').html('Введите ваш Email:');
    $('.buttons_form').hide();
    $('.form_block').show();
    $('.form_template').hide();
    $('.form_email').show();
  });

  $(document).on('click','.js-close-share-social',function(){
    $('.finish_share').hide();
    $('.finish_content').show();
  });

  $(document).on('click','.js-show-share-social',function(){
    $('.finish_share').show();
    $('.finish_content').hide();
  });

  $('.js-close-form').click(function(){
    $('.js-pop-up-forms').hide();
    $('.js-pop-up').css('display','flex');
  });

  $(document).on('click','.video_container_next',function(){

    if(test_id==1){
      var duration=94;
    }
    if(test_id==2){
      var duration=26;
    }
    if(test_id==3){
      var duration=29;
    }
    if(test_id==4){
      var duration=36;
    }
    if(test_id==5){
      var duration=44;
    }
    if(test_id==6){
      var duration=23;
    }

    if(type_video=='video_1_true'){
      var duration=25;
    }
    if(type_video=='video_2_true'){
      var duration=35;
    }
    if(type_video=='video_3_true'){
      var duration=30;
    }

    if(type_video=='video_1_false'){
      var duration=19;
    }
    if(type_video=='video_2_false'){
      var duration=29;
    }
    if(type_video=='video_3_false'){
      var duration=24;
    }

    var video=$('.video').get(0);
    video.currentTime=duration;
  });




  $(document).on('click','.js-repeat',function(){
    type_video='video';

    $('.video_container').removeClass('video_container_error_time');
    $('.video_container').removeClass('video_container_important_time');
    $('.video_container').removeClass('video_container_answer_false');
    $('.video_container').removeClass('video_container_answer_true');
    $('.video_container_pop_up_parent_error').removeClass('video_container_pop_up_parent_error');


    $('.video_container_timer').hide();
    work_timer=false;
    show_question=false;

    initialize_video();
  });
  $(document).on('click','.js-repeat-all',function(){
    test_id=1;
    type_video='video';

    $('.video_container').removeClass('video_container_error_time');
    $('.video_container').removeClass('video_container_important_time');
    $('.video_container').removeClass('video_container_answer_false');
    $('.video_container').removeClass('video_container_answer_true');
    $('.video_container_pop_up_parent_error').removeClass('video_container_pop_up_parent_error');


    $('.video_container_timer').hide();
    work_timer=false;
    show_question=false;


    $('.video_container').removeClass('video_container_finish');
    $('.schet').text('СЧЕТ:');
    $('.js-repeat-all').removeClass('js-repeat-all').addClass('js-repeat');

    browser_id=Math.floor((Math.random() * 999999999999) + 999999);

    initialize_video();
  });

  $(document).on('click','.js-set-next-test',function(){
    if(!$(this).is('.button_disable')){

      $('.video_container').removeClass('video_container_error_time');
      $('.video_container').removeClass('video_container_important_time');
      $('.video_container').removeClass('video_container_answer_false');
      $('.video_container').removeClass('video_container_answer_true');
      $('.video_container_pop_up_parent_error').removeClass('video_container_pop_up_parent_error');
      $('.video_container_timer').hide();

      video.pause();

      setTimeout(function(){

        work_timer=false;
        show_question=false;


        if(test_id==6){
          test_id='result';
        }else{
          test_id++;
        }
        type_video='video';
        initialize_video();
      });

    }
  });

  $(document).on('click','.js-set-answer',function(){



    if(!$(this).is('.button_disable')){

      $('.video_container').removeClass('video_container_error_time');
      $('.video_container').removeClass('video_container_important_time');
      $('.video_container').removeClass('video_container_answer_false');
      $('.video_container').removeClass('video_container_answer_true');
      $('.video_container_pop_up_parent_error').removeClass('video_container_pop_up_parent_error');
      $('.video_container_timer').hide();

      video.pause();

      var code=$(this).attr('code');

      setTimeout(function(){


        work_timer=false;
        show_question=false;

        $('.video_container_score_button_repeat').hide();

        $.ajax({
          type     :'POST',
          cache    : false,
          data: {
            test_id: test_id,
            code: code,
            browser_id: browser_id,
          },
          url  : '/api/set_question',
          success  : function(response) {

            range_score(response.range,response.score);

            if(response.result==true){
              $('.video_container').addClass('video_container_answer_true');
              $('.js-pop-up .video_container_pop_up').html('<div class="answer_title">Отлично!</div>');
            }else{
              $('.video_container').addClass('video_container_answer_false');
              $('.js-pop-up .video_container_pop_up').html('<div class="answer_title">Неверно!</div>');
            }


            if(test_id==1){
              var post='Руководитель отдела продаж  компании <a target="_blank" href="http://www.tehnoleasing.ru/">ТЕХНО Лизинг</a>'
            }
            if(test_id==2){
              var post='Заместитель генерального директора компании <a href="https://tetron.ru/" target="_blank">Тетрон</a>'
            }
            if(test_id==3){
              var post='Технический эксперт <a href="https://savefuel.michelin.ru/#calculator" target="_blank">Мишлен</a>'
            }

            if(post!=undefined){
              $('.js-pop-up .video_container_pop_up').append('<div class="answer_info_post"><span>'+post+'</span></div>');
            }

            $('.js-pop-up .video_container_pop_up').append('<div class="clear"></div>');

            $('.js-pop-up .video_container_pop_up').append('<div class="answer_comment"><div class="answer_comment_arrow"></div><div class="answer_comment_scroll">'+response.answer+'</div></div>');

            if(test_id==3){
              $('.js-pop-up .video_container_pop_up').append('<div class="video_container_pop_up_buttons">\
                <div class="button button_white button_pdf js-open-form-pdf gtm-get-pdf" code="0">\
                  Получить pdf\
                </div>\
                <a href="https://savefuel.michelin.ru/?utm_source=ati_su_Michelin&utm_medium=DIS&utm_content=MICHELIN/HeavyTruck/Brand/PDT/XMulti&utm_campaign=HeavyTruck-LDS-XMulti&utm_term=video#calculator" target="_blank" class="button button_white button_calc" code="0">\
                  калькулятор<br>экономии\
                </a>\
              </div>\
              <div class="button button_arrow js-set-next-test" code="1" style="margin-top: 15px">\
                Продолжить\
              </div>');

            }else{
              if(test_id==6){
                $('.js-pop-up .video_container_pop_up').append('\
                <div class="video_container_pop_up_buttons">\
                  <div class="button button_white button_pdf js-open-form-pdf" code="0">\
                    прочитать<br>почему\
                  </div>\
                  <div class="button button_arrow js-set-next-test" code="1">\
                    Продолжить\
                  </div>\
                </div>');
              }else{
                $('.js-pop-up .video_container_pop_up').append('\
                <div class="video_container_pop_up_buttons">\
                  <div class="button button_white button_pdf js-open-form-pdf gtm-get-pdf" code="0">\
                    Получить pdf\
                  </div>\
                  <div class="button button_arrow js-set-next-test" code="1">\
                    Продолжить\
                  </div>\
                </div>');
              }

            }


            $('.answer_comment_scroll').scrollbar();



          },
          error: function(jqxhr, status, errorMsg) {}
        });
      });
    }

  });

  var range_timer;

  function range_score(range,score ){

    clearTimeout(range_timer);

    $('.video_container_score_range').remove();

    var range_int=parseInt(range);

    if(range_int>0){
      $('.video_container_score').append('<div class="video_container_score_range video_container_score_range_plus">'+range+' <span class="rub">i</span></div>');
    }

    if(range_int<0){
      $('.video_container_score').append('<div class="video_container_score_range video_container_score_range_minus">'+range+' <span class="rub">i</span></div>');
    }

    $('.video_container_score_range').addClass('video_container_score_range_animation');

    range_timer=setTimeout(function(){
      $('.video_container_score_range').remove();
      $('.score').html(score+' <span class="rub">i</span>').attr('score',score.replace(/\s+/g, ''));
    },2000);


  }


  var test_id=1;
  var type_video='video';
  var work_timer=false;
  var show_question=false;


  var video;
  var browser_id=Math.floor((Math.random() * 999999999999) + 999999);

  function initialize_video(){

    $('.video_loader').css('height','inherit');


    if($('body').width()<960){
      $('body').animate({
        scrollTop: 0,
      }, 800);
    }

    if($('body').width()<750){
      $('.js-pop-up-preview').show();
    }

    $('.video_container_timer').hide();
    $('.js-pop-up-time-end').hide();
    $('.js-pop-up').hide();
    $('.js-pop-up-forms').hide();
    $('.video_container').removeClass('video_container_error_time');
    $('.video_container').removeClass('video_container_important_time');
    $('.video_container').removeClass('video_container_answer_false');
    $('.video_container').removeClass('video_container_answer_true');
    $('.video_container_pop_up_parent_error').removeClass('video_container_pop_up_parent_error');

    //Определяем папку
    var name_folder='test_'+test_id;

    if(test_id==1 && type_video=='video'){
      $('.score').html('10 500 000 <span class="rub">i</span>');
      $('.video_container_score').hide();
      $('.video_container_score_button_repeat').hide();
    }else{
      $('.video_container_score_button_repeat').show();
      $('.video_container_score').show();
    }

    $.ajax({
      type     :'POST',
      cache    : false,
      data: {
        test_id: test_id,
        url: $('input[name="url"]').val(),
        browser_id: browser_id,
      },
      url  : '/api/get_question',
      success  : function(response) {
        $('.js-pop-up .video_container_pop_up').html(response);

        $('.js-pop-up-preview .video_container_pop_up_title').html($('.js-pop-up .video_container_pop_up_title').html());
        $('.js-pop-up-preview .video_container_pop_up_description').html($('.js-pop-up .video_container_pop_up_description').html());
      },
      error: function(jqxhr, status, errorMsg) {}
    });

    //Определяем результирующее видео
    if(test_id=='result'){
      var result_score=parseInt($('.score').attr('score'));
      if(result_score<10000000){
        type_video='video_1_'+$('input[name="logo"]').val();
      }else{
        if(result_score<13999999 && result_score>10000001){
          type_video='video_2_'+$('input[name="logo"]').val();
        }else{
          if(result_score>14000000){
            type_video='video_3_'+$('input[name="logo"]').val();
          }
        }
      }
    }

    //Вставляем html5 контейнер

    $('.video_loader').html('\
      <div class="video_container_border"></div>\
      <div class="video_container_timeline">\
        <div class="video_container_timeline_loader"></div>\
      </div>\
      <video class="video" playsinline width="100%" poster="videos/'+name_folder+'/'+type_video+'.jpg">\
        <source src="videos/'+name_folder+'/'+type_video+'.mp4" type="video/mp4">\
        <source src="videos/'+name_folder+'/'+type_video+'.webm" type="video/webm">\
        <source src="videos/'+name_folder+'/'+type_video+'.ogv" type="video/ogg">\
      </video>');

    if($('input[name="logo"]').val()=='true'){
      $('.video_loader').append('<div class="michelin-logo">michelin</div>');
    }


    $('.video_loader').append('<div class="video_container_next" test_id="'+test_id+'">Пропустить</div>');

    if($('body').width()>750){
      $('.video_loader').append('<div class="video_opacity"></div>');
    }



    //Запускаем видео
    video=$('.video').get(0);
    video.play();


    //Отслеживаем таймлайн
    $('.video').on(
    "timeupdate",
    function(event){
      if(test_id==1){
        if(this.currentTime>26 && this.currentTime<28 && show_question==false){
          $('.video_container_score').show();
          $('.video_container_score_button_repeat').show();
        }
        if(this.currentTime>30 && this.currentTime<32 && show_question==false){
          $('.video_container_timer').show();
        }else{
          if(this.currentTime<90){
            $('.video_container_timer').hide();
          }
        }
        var duration=94;
      }
      if(test_id==2){
        var duration=26;
      }
      if(test_id==3){
        var duration=29;
      }
      if(test_id==4){
        var duration=36;
      }
      if(test_id==5){
        var duration=44;
      }
      if(test_id==6){
        var duration=23;
      }
      if(type_video=='video_1_true'){
        var duration=25;
      }
      if(type_video=='video_2_true'){
        var duration=35;
      }
      if(type_video=='video_3_true'){
        var duration=30;
      }

      if(type_video=='video_1_false'){
        var duration=19;
      }
      if(type_video=='video_2_false'){
        var duration=29;
      }
      if(type_video=='video_3_false'){
        var duration=24;
      }

      //Записываем процент загрузки видео

      var percent_video=(this.currentTime/duration)*100;
      $('.video_container_timeline_loader').width(percent_video+'%');
      //Делаем действия взависимости от процента или времени загрузки
      if(percent_video>=100 && show_question==false){

        $('.js-pop-up-preview').hide();

        show_question=true;
        $('.video_container_timeline').hide();
        $('.video_container_next').hide();
        $('.video_container_score').show();

        if(test_id=='result'){
          //$('.video_loader').css('height',$('.video_loader').height());
          //$('.video_loader video').remove();
          $('.video_container_pop_up_title').remove();
          $('.video_container_pop_up_description').remove();
          $('.video_loader').css('background-image','url(/videos/test_result/background/'+type_video+'.jpg)');
          $('.video_container').addClass('video_container_finish');
          $('.js-repeat').removeClass('js-repeat').addClass('js-repeat-all');
          $('.schet').text('ВАШ РЕЗУЛЬТАТ:');

          if($('body').width()>750){
            $('.video_container_pop_up').css({'margin-top':'1000px','transition':'0.5s'});
            setTimeout(function(){
              $('.video_container_pop_up').css({'margin-top':'0px'});
              setTimeout(function(){
                $('.video_container_pop_up').removeAttr('style');
              },500);
            });
          }
        }else{
          $('.video_container').removeClass('video_container_finish');
          $('.schet').text('СЧЕТ:');
          $('.js-repeat-all').removeClass('js-repeat-all').addClass('js-repeat');
        }

        $('.js-pop-up').css('display','flex');

        if(test_id<=6){
          $('.video_container_timer').show();
          $('.video_container_timer_time').text(30).attr('time',30);
          work_timer=true;
          setTimeout(function(){
            start_timer();
          },1000);
        }else{
          $('.video_container_timer').hide();
        }

      }
    });

  }



  function start_timer(){

    var now_time=parseInt($('.video_container_timer_time').attr('time'));

    now_time--;
    if(now_time<=30 && now_time>=0){
      if(now_time<10){
        text_time='0'+now_time;
      }else{
        text_time=now_time;
      }
      $('.video_container_timer_time').text(text_time).attr('time',now_time);
    }else{
      $('.video_container_timer_time').attr('time',now_time);
    }

    if(now_time==10){
      $('.video_container').removeClass('video_container_error_time');
      $('.video_container').addClass('video_container_important_time');
    }

    if(now_time==0){
      $('.video_container').addClass('video_container_error_time');
      $('.video_container').removeClass('video_container_important_time');
    }
    if(work_timer==true){
      if(now_time>0){
        setTimeout(function(){
          start_timer();
        },1000);
      }else{
        if(now_time>-30){
          $('.js-pop-up').addClass('video_container_pop_up_parent_error');
          if(now_time%3==0){
            $.ajax({
              type     :'POST',
              cache    : false,
              data: {
                browser_id: browser_id,
              },
              url  : '/api/minus_score',
              success  : function(response) {
                range_score(response.range,response.score);
              },
            });
          }

          setTimeout(function(){
            start_timer();
          },1000);
        }else{

          video.pause();
          $('.js-pop-up').hide();
          $('.js-pop-up-time-end').css('display','flex');

        }
      }
    }

  }

  $('.first_slide_button').click(function(){
    $('.first_slide').slideUp();
    $('.video_slide').slideDown();
    setTimeout(function(){
      $('.first_slide').remove();
      //$('.video_slide').find('.content').unwrap();
    },300);

    initialize_video();

    ym(57516034,'reachGoal','button1_prover');

  });


  $(document).on('click','.button_check',function(){
    var check_icon=$(this).find('.button_check_icon');

    if(check_icon.is('.active')){
      check_icon.removeClass('active');
    }else{
      check_icon.addClass('active');
    }

    var result='';

    $('.button_check_icon.active').each(function(){
      result+=$(this).attr('code');
    });

    if(result.length==0){
      $('.button_test_2').addClass('button_disable');
    }else{
      $('.button_test_2').removeClass('button_disable');
    }

    $('.button_test_2').attr('code',result);


  });


  // $(document).on('click','.button_nalog',function(){
  //   var check_icon=$(this).find('.button_check_icon');
  //
  //   if(check_icon.is('.active')){
  //     check_icon.removeClass('active');
  //   }else{
  //     check_icon.addClass('active');
  //   }
  //
  //   if($('.button_check_icon.active').length==3){
  //     $('.js-set-next-test').removeClass('button_disable');
  //   }
  //
  //
  // });



});




/*Куки*/
function set_cookie( name, value, expires, path, domain, secure )
{
// set time, it's in milliseconds
var today = new Date();
today.setTime( today.getTime() );
path='/';
/*
if the expires variable is set, make the correct
expires time, the current script below will set
it for x number of days, to make it for hours,
delete * 24, for minutes, delete * 60 * 24
*/
if ( expires )
{
expires = expires * 1000;
}
var expires_date = new Date( today.getTime() + (expires) );

  document.cookie = name + "=" +escape( value ) +
  ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
  ( ( path ) ? ";path=" + path : "" ) +
  ( ( domain ) ? ";domain=" + domain : "" ) +
  ( ( secure ) ? ";secure" : "" );
}

function set_cookie_uri( name, value, expires, path, domain, secure )
{
// set time, it's in milliseconds
var today = new Date();
today.setTime( today.getTime() );
path='/';
/*
if the expires variable is set, make the correct
expires time, the current script below will set
it for x number of days, to make it for hours,
delete * 24, for minutes, delete * 60 * 24
*/
if ( expires )
{
expires = expires * 1000;
}
var expires_date = new Date( today.getTime() + (expires) );

  document.cookie = name + "=" +escape( encodeURIComponent(value) ) +
  ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
  ( ( path ) ? ";path=" + path : "" ) +
  ( ( domain ) ? ";domain=" + domain : "" ) +
  ( ( secure ) ? ";secure" : "" );
}


function delete_cookie ( cookie_name )
{
  var cookie_date = new Date ( );  // Текущая дата и время
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}
