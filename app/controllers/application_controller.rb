class ApplicationController < ActionController::Base

  skip_before_action :verify_authenticity_token


  def share

    @title="Я управлял компанией по грузоперевозкам и принимал непростые решения  — справитесь лучше?"

    render 'index/share', layout: 'share'
  end

  def minus_score

    test_result=TestResult.find_by(browser_id: params[:browser_id])
    test_result.score=test_result.score-90000
    test_result.save

    render json: {
      range: '-90 000',
      score: ActiveSupport::NumberHelper.number_to_delimited(test_result.score).gsub(',',' '),
    }

  end

  def get_question

    test_result=TestResult.find_by(browser_id: params[:browser_id])
    if test_result.blank?

      TestResult.create(
        browser_id: params[:browser_id],
        score: 10500000,
      )
      test_result=TestResult.find_by(browser_id: params[:browser_id])
    end

    if params[:test_id]=="1"
      test_result.score=10500000
      test_result.save
    end

    render "layouts/questions/test_#{params[:test_id]}", layout: nil
  end

  def send_form

    test_result=TestResult.find_by(browser_id: params[:browser_id])
    if params[:phone].present?
      test_result.phone="#{test_result.phone}#{params[:phone]},"
      request_data='"phone": "'+params[:phone]+'",'
      code_event="ecb04360-e897-47d9-9a13-2cd370765eaf"
    end
    if params[:email].present?
      test_result.email="#{test_result.email}#{params[:email]},"
      request_data='"email": "'+params[:email]+'",'
      code_event="46285901-ae07-4b70-8bd3-cb180d9e889f"
    end
    test_result.save


    if params[:type_form]=='consult'
      code_event="d7a39236-7196-4ac9-a51e-cf5403a17c49"
    end

    response=JSON.parse(%x{curl --location --request POST 'https://api-michelin.konnektu.ru/registration' \
                    --header 'Content-Type: application/json' \
                    --data-raw '{
                          "request": {
                            "sourceId": "96ed3e34-4dcc-4a07-b660-5679daf01299",
                            "sourceSecretKey": "Cd4FTfCNm7vaGMhT",
                            "userData": {
                              #{request_data}
                              "surname": "",
                              "givenName": "",
                              "middleName": "",
                              "userConsents": [
                                {
                                  "consentId": "519203d4-ac96-4ca1-a881-281c93b9a56e",
                                  "value": true
                                },
                                {
                                  "consentId": "cd45d3c0-1ae8-42f1-84ec-a3478e294ff9",
                                  "value": true
                                }
                              ]
                            }
                          }
                        }'
                      })



    if response["userData"].blank?

      response=JSON.parse(%x{curl --location --request POST 'https://api-michelin.konnektu.ru/login' \
                      --header 'Content-Type: application/json' \
                      --data-raw '{
                            "request": {
                              "sourceId": "96ed3e34-4dcc-4a07-b660-5679daf01299",
                              "sourceSecretKey": "Cd4FTfCNm7vaGMhT",
                              "userData": {
                                #{request_data}
                              }
                            }
                          }'
                        })



    end

    user_id=response["userData"]['userId']
    secret_key=response["userData"]['secretKey']

    response=JSON.parse(%x{curl --location --request POST 'https://api-michelin.konnektu.ru/adduserevent' \
                    --header 'Content-Type: application/json' \
                    --data-raw '{
                          "request": {
                            "sourceId": "96ed3e34-4dcc-4a07-b660-5679daf01299",
                            "sourceSecretKey": "Cd4FTfCNm7vaGMhT",
                            "userData": {
                              "userId" : "#{user_id}",
                              "secretKey" : "#{secret_key}",
                            },
                            "campaign" : {
                              "event" : {
                                "id" : "#{code_event}",
                              }
                            }
                          }
                        }'
                      })



    render plain: 'ok'

  end

  def set_question


    test_result=TestResult.find_by(browser_id: params[:browser_id])
    if test_result.blank?

      TestResult.create(
        browser_id: params[:browser_id],
        score: 10500000,
      )
      test_result=TestResult.find_by(browser_id: params[:browser_id])
    end

    if params[:test_id]=="1"
      test_result.score=10500000
      if params[:code]=='0'
        result=true
        range='+2 400 000'
        answer='Продать старые автомобили и&nbsp;взять в&nbsp;лизинг новые&nbsp;&mdash; отличная идея. Водители будут комфортнее работать&nbsp;&mdash; коэффициент выхода на&nbsp;линию составит&nbsp;97%, а&nbsp;вы&nbsp;сможете заключить более выгодные контракты с&nbsp;более прибыльными заказчиками. При этом, когда заключаете лизинговый контракт на&nbsp;5&nbsp;лет, например, в&nbsp;компании <a target="_blank" href="http://www.tehnoleasing.ru/">ТЕХНО Лизинг</a>, в&nbsp;итоге экономите почти 500 тысяч рублей в&nbsp;год благодаря меньшим налогам на&nbsp;прибыль и&nbsp;НДС.'
      end
      if params[:code]=='1'
        result=false
        range='-1 600 000'
        answer='Мы&nbsp;понимаем, что старые машины&nbsp;&mdash; ваши любимцы, но&nbsp;ради бизнеса с&nbsp;ними лучше расстаться. По данным компании <a target="_blank" href="http://www.tehnoleasing.ru/">ТЕХНО Лизинг</a> эксплуатационные расходы за&nbsp;5&nbsp;лет достигнут почти 2,5 миллионов рублей, а&nbsp;коэффициент выхода на&nbsp;линию у&nbsp;старых машин составляет всего 70%. При этом, вы&nbsp;не&nbsp;сэкономите на&nbsp;налогах ни&nbsp;копейки&nbsp;&mdash; с&nbsp;новыми машинами, взятыми в&nbsp;лизинг, экономия составит почти 3&nbsp;миллиона рублей за&nbsp;5&nbsp;лет. '
      end
    end

    if params[:test_id]=="2"
      if params[:code]=='01'
        range='+4 140 000'

        answer='В&nbsp;этом вопросе оба ответа правильные. Контроль за&nbsp;расходами проходит в&nbsp;два этапа&nbsp;&mdash; в&nbsp;первую очередь вы&nbsp;учите водителей расходовать меньше топлива. Затем устанавливаете телематику, например, от&nbsp;компании <a href="https://tetron.ru/" target="_blank">Тетрон</a>, чтобы контролировать расход&nbsp;&mdash; в&nbsp;автопарке на&nbsp;60&nbsp;машин это позволит сэкономить более 3&nbsp;миллионов в&nbsp;год. В&nbsp;итоге выигрывают все: вы&nbsp;снижаете расходы, а&nbsp;водители повышают профессионализм на&nbsp;обучающих курсах и&nbsp;становятся более ценными специалистами на&nbsp;рынке. '

        result=true

      else

        result=false

        if params[:code]=='0'
          range='+3 350 000'
        end
        if params[:code]=='1'
          range='+790 000'
        end

        answer='Вы&nbsp;можете выбрать что-то одно, но&nbsp;не&nbsp;факт, что это сработает. Тут есть всего два варианта. Либо вы&nbsp;жёстко следите за&nbsp;тем, как расходуется топливо, но&nbsp;водители продолжают его тратить. Либо водители стараются экономить, но&nbsp;вы&nbsp;не&nbsp;можете отслеживать, как они это делают. Логичнее будет вложиться в&nbsp;оба направления сразу&nbsp;&mdash; так вы&nbsp;повысите лояльность водителей&nbsp;&mdash; людям нравится работать в&nbsp;компаниях, которые заботятся об&nbsp;их&nbsp;развитии. Также получится сэкономить на&nbsp;топливе&nbsp;&mdash; например, телематика от&nbsp;компании <a href="https://tetron.ru/" target="_blank">Тетрон</a> в&nbsp;автопарке на&nbsp;60&nbsp;машин позволяет сэкономить более 3&nbsp;миллионов в&nbsp;год.'

      end



    end


    if params[:test_id]=="3"
      if params[:code]=='1'
        result=true
        range='+5 400 000'
        answer='Не&nbsp;стоит бояться тратить деньги на&nbsp;энергосберегающие шины, такие как <a target="_blank" href="https://savefuel.michelin.ru/#calculator">MICHELIN X&nbsp;MULTI ENERGY</a>. Каждый грузовик на&nbsp;этих шинах может тратить на&nbsp;2,4 литра топлива меньше на&nbsp;каждые 100&nbsp;километров, что за&nbsp;три года позволит сэкономить до&nbsp;450 тысяч рублей. Переобувать весь парк разом не&nbsp;обязательно или можно <a href="https://neoleasing.com/" target="_blank">взять шины в&nbsp;лизинг</a>. При этом, каждый грузовик с&nbsp;новыми шинами сразу может начать экономить вам деньги. Разные комбинации ошиновки помогут добиться нужного уровня экономии в&nbsp;сочетании с&nbsp;<a target="_blank" href="https://truck.michelin.ru/news/prostie-resheniya-slozhnih-problem/">пробегом и&nbsp;сцеплением</a>.'

      end
      if params[:code]=='0'
        result=false
        range='-3 600 000'
        answer='Допустим, вы&nbsp;сэкономили&nbsp;50% на&nbsp;шинах и&nbsp;купили более дешевый вариант. В&nbsp;итоге вы&nbsp;увеличили расходы на&nbsp;ТО, топливо, штрафы и&nbsp;даже зарплату водителей&nbsp;&mdash; это приведёт вас к&nbsp;большим потерям. К&nbsp;тому&nbsp;же, более дешёвая шина, в&nbsp;отличие от&nbsp;<a target="_blank" href="https://savefuel.michelin.ru/#calculator">MICHELIN X&nbsp;MULTI ENERGY</a>, может быстрее изнашиваться&nbsp;&mdash; в&nbsp;итоге расходы увеличиваются, а&nbsp;пробег остаётся небольшим. В&nbsp;конце концов скупой заплатит дважды&nbsp;&mdash; как обычно.'
      end
    end

    if params[:test_id]=="4"
      if params[:code]=='0'
        result=true
        range='+3 000 000'
        answer='Хороший ответ! Водители, зарплата которых зависит от&nbsp;количества доставок или пройденного километража, мотивированы работать больше и&nbsp;лучше, чем водители, которые получают оклад. Если&nbsp;же вы&nbsp;будете давать премии за&nbsp;аккуратное вождение и&nbsp;меньший расход топлива, то&nbsp;водители будут бережнее относится к&nbsp;автомобилям, а&nbsp;вы&nbsp;станете меньше средств тратить на&nbsp;ремонт, ТО&nbsp;и&nbsp;топливо. Грамотная система мотивации в&nbsp;компании с&nbsp;400 сотрудниками позволяет ей&nbsp;в&nbsp;месяц сэкономить до&nbsp;400 тысяч рублей, а&nbsp;в&nbsp;год&nbsp;&mdash; до&nbsp;5&nbsp;миллионов. '
      end
      if params[:code]=='1'
        result=false
        range='-2 000 000'
        answer='Уверенность водителей в&nbsp;завтрашнем дне&nbsp;&mdash; хороший фактор для поддержания их&nbsp;лояльности. Но&nbsp;компании-лидеры в&nbsp;нашей сфере предпочитают платить за&nbsp;результат&nbsp;&mdash; водитель, который работает гораздо лучше, но&nbsp;получает такой&nbsp;же оклад, как остальные, скорее уйдёт к&nbsp;конкурентам. Зато водители, которые видят, что за&nbsp;лучшие результаты получают больше, стараются развиваться и&nbsp;в&nbsp;итоге более лояльны вашему бизнесу.'
      end
    end

    if params[:test_id]=="5"
      if params[:code]=='0'
        result=true
        range='+3 150 000'
        answer='Если в&nbsp;вашем автопарке 60&nbsp;машин, то&nbsp;обслуживание у&nbsp;официальных дилеров после покупки обойдётся дороже&nbsp;&mdash; до&nbsp;350 тысяч в&nbsp;год, но&nbsp;при этом вы&nbsp;снижаете расходы на&nbsp;ремонт&nbsp;&mdash; вплоть до &minus;20%. В&nbsp;итоге вы&nbsp;спите спокойно и&nbsp;знаете, что не&nbsp;потеряете деньги из-за неоригинальных запчастей, мастеров без сертификации производителя или отсутствия гарантии на&nbsp;ремонтные работы&nbsp;&mdash; экономия может составить около 700 тысяч рублей в&nbsp;год. '
      end
      if params[:code]=='1'
        result=false
        range='-2 100 000'
        answer='Если приложить большие усилия, то&nbsp;мы&nbsp;сможем сократить наши затраты на&nbsp;ТО до&nbsp;30% в&nbsp;год. В&nbsp;процентах звучит хорошо, но&nbsp;в&nbsp;абсолютных величинах сумма составит до&nbsp;350 тысяч в&nbsp;год. Это не&nbsp;те&nbsp;деньги, ради которых стоит рисковать и&nbsp;устанавливать неоригинальные запчасти&nbsp;&mdash; в&nbsp;перспективе есть шансы потерять больше на&nbsp;ремонт. '
      end
    end

    range_number=range.gsub(" ","").to_i

    test_result["test_#{params[:test_id]}"]=result
    test_result.score=test_result.score+range_number

    test_result.save

    render json: {
      result: result,
      range: range,
      score: ActiveSupport::NumberHelper.number_to_delimited(test_result.score).gsub(',',' '),
      answer: answer,
    }

  end


  after_action :allow

  private


      def allow
        response.headers['X-Frame-Options'] = 'ALLOW-FROM https://ati.su'
      end

end
